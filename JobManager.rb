require "open3"
require "time"

class Job
    @@njobs=0
    @@id="NONE"
    attr_accessor :opts
    attr_accessor :procs
    attr_accessor :defs
    attr_accessor :tag
    attr_accessor :files
    attr_accessor :model
    attr_accessor :sets
    attr_accessor :name
    attr_accessor :opath
    def initialize
        time = Time.new
        @procs = []
        @opts = []
        @model = "NONE"
        @files = []
        @sets = []
        @name="NONE"
        # Save default optiones that will be written in all files
        @def_opts = [
            "#************************************************************",
            "#*                        MadGraph 5                        *",
            "#*                                                          *",
            "#*                *                       *                 *",
            "#*                  *        * *        *                   *",
            "#*                    * * * * 5 * * * *                     *",
            "#*                  *        * *        *                   *",
            "#*                *                       *                 *",
            "#*                                                          *",
            "#*                                                          *",
            "#*    The MadGraph Development Team - Please visit us at    *",
            "#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *",
            "#*                                                          *",
            "#************************************************************",
            "#*                                                          *",
            "#*               Command File for MadGraph 5                *",
            "#*                                                          *",
            "#*     run as ./bin/mg5  filename                           *",
            "#*                                                          *",
            "#************************************************************",
            "",
            "#************************************************************",
            "#    File automatically generated with JogManager class    ",
            "#    On: #{time.strftime("%Y-%m-%d %H:%M")}          ",
            "#************************************************************"
        ]
        # Default defines. These can be overwritten if desired
        @defs = {
            "p" => "g u c d s u~ c~ d~ s~",
            "j" => "g u c d s u~ c~ d~ s~",
            "l+" => "e+ mu+",
            "l-"=> "e- mu-",
            "vl" => "ve vm vt",
            "vl~" => "ve~ vm~ vt~"
        }
    end

    def set_defines(dd)
        @defs.merge!(dd)
    end

    def set_ID(id)
        @id = id
    end

    def set(ss)
        sets << ss
    end

    def info
        puts "Job information:"
    end

    def create_card
        @name = @@njobs.to_s if @name == "NONE"
        fname="#{@opath}/proc_#{@id}_#{@name}.dat"
        file = File.open(fname,"w")
        @def_opts.each {|l| file.write("#{l}\n")}
        mm = "sm"
        if @model != "NONE"
            mm = @model 
        end
        file.write("import model #{mm}\n")
        @defs.each do |key,value|
            file.write("define #{key} = #{value}\n")
        end
        abort("No process has been defined.") if @procs.length == 0
        file.write("generate #{@procs[0]}\n")
        if @procs.length > 1
            @procs[1..-1].each { |x| file.write("add process #{x}\n")}
        end
        file.write("output #{@id}_#{@name}\nlaunch -i\n")
        id = -1
        @opts.each_with_index do |l,i| 
            if l.include? "multi_run"
                id = i
            end
        end
        if id != -1
            file.write("#{@opts[id]}\n")
            @opts.delete_at(id)
        else 
            file.write("generate_events\n")
        end
        @opts.each {|l| file.write("#{l}\n")}
        @files.each {|l| file.write("#{l}\n")}
        #file.write("set run_tag #{@tag}\n") if @tag != "NONE"
        @sets.each {|s| file.write("set #{s}\n")}
        file.close
        return fname
    end
end

class Numeric
    def duration
        secs  = self.to_int
        mins  = secs / 60
        hours = mins / 60
        days  = hours / 24

        if days > 0
            "#{days}d:#{hours % 24}h"
        elsif hours > 0
            "#{hours}h:#{mins % 60}m"
        elsif mins > 0
            "#{mins}m:#{secs % 60}s"
        elsif secs >= 0
            "#{secs}s"
        end
    end
end

class JobManager
    attr_accessor :max_jobs
    def initialize(param={})
        @params = {
            :email=>"NO",
            :verbose=>false,
            :ID=>"NONE",
            :max=>16,
            :submax=>-1,
            :wait => 60,
            :output_path => "."
        }.merge(param)
        @jobs = []
        @procs = []
        @finished = 0
        if @params[:output_path] != "."
            puts "Creating output path #{@params[:output_path]}"
            `mkdir -p #{@params[:output_path]}`
        end
    end

    def add_job
        job = Job.new
        job.set_ID(@params[:ID])
        yield(job)
        @jobs << job
        if @params["verbose"]
            puts "Added job: "
            job.info
        end
    end

    def launch(param = {:test=>false,:run=>false})
        nb = 0
        sleep = 0
        @startime = Time.now
        @jobs.each do |job|
            script = File.open("mg5jobs.sh","w")
            file = job.create_card
            if !File.exists?("./bin/mg5_aMC")
                abort("ERROR: The executable ./bin/mg5_aMC has not been found. Run again from the MadGraph location")
            end
            script.write("#!/bin/bash
            cd $PBS_O_WORKDIR\n")
            if !param[:run]
                script.write("sleep #{sleep}\n")
                sleep += 10
            end
            script.write("./bin/mg5_aMC #{file} > #{@params[:output_path]}/output_#{@params[:ID]}_#{job.name}\n")
            script.close
            if !param[:test]
                `chmod +x mg5jobs.sh`
                #while nb >= @max_jobs
                #    sleep(10)
                if !param[:run]
                    if checksubmit
                        i,o,e,t = Open3.popen3("qsub mg5jobs.sh")
                        @procs << o.read.delete("\n")
                    end
                else
                    if checkrunning
                        i,o,e,t = Open3.popen3("./bin/mg5_aMC #{file} > #{@params[:output_path]}/output_#{@params[:ID]}_#{job.name}")
                        @procs << t
                        #i,o,e,t = Open3.popen3("echo hola")
                        #@procs << t
                    end
                end
            end
        end
    end

    def checksubmit
        loop do
            if @procs.length <  @params[:submax] or @params[:submax] == -1
                return true
            else
                pfin = []
                @procs.each do |id|
                    i,oe,t = Open3.popen2e("qstat #{id}")
                    oe.each_line do |line|
                        status = "R"
                        if line.start_with? id.split(".")[0]
                            status = line.split()[4]
                        end
                        if line.include? "Unknown" or status == "E"
                            pfin << id
                            @finished += 1
                        end
                    end
                end
                pfin.each {|x| @procs.delete x}
                return true if pfin.length > 0 
            end
            print "\rWaiting: iddle[#{@jobs.length - @procs.length - @finished}], running[#{@procs.length}], finished[#{@finished}]\s\t Elapsed time: #{(Time.now - @startime).duration}"
            STDOUT.flush
            sleep @params[:wait]
        end
    end

    def checkrunning
        loop do
            if @procs.length < @params[:max]
                return true
            else
                @procs.each do |p|
                    begin
                        Process.getpgid(p.pid)
                    rescue Errno::ESRCH
                        @procs.delete p
                        @finished += 1
                    end
                end
            end
            print "\rWaiting: iddle[#{@jobs.length - @procs.length - @finished}], running[#{@procs.length}], finished[#{@finished}]\s\t Elapsed time: #{(Time.now - @startime).duration}"
            STDOUT.flush
            sleep @params[:wait]
        end
    end

    def generate_events(opt={})
        gopt = {
            :nevents => 30000,
            :multi_run => 1,
            :test => false,
            :run => false,
            :mcnlo => false
        }.merge(opt)
        multirun = "generate_events"
        if gopt[:multi_run] > 1
            multirun = "multi_run #{gopt[:multi_run]}"
        end
        puts "Statistic extension for #{@params[:ID]} up to #{gopt[:nevents]*gopt[:multi_run]}. Will be done in #{gopt[:multi_run]} runs."
        sleep = 0
        # Loop for multi_runs
        @jobs.each do |job|
            #  Create multirun file
            # Check if run folder exists
            if !Dir.exists? "#{@params[:ID]}_#{job.name}"
                abort "You are requesting an statistics extension for #{@params[:ID]}_#{job.name} but this folder doesn't exists. Create the process folder before using the JobManager::launch function."
            end
            mrfile = File.open("#{@params[:ID]}_#{job.name}/genevs_config.txt","w")
            #mrfile.write("#{multirun}\n")
            mrfile.write("set nevents #{gopt[:nevents]}\n")
            mrfile.close
            # Create script to launch
            script = File.open("mg5generate.sh","w")
            script.write("#!/bin/bash
            cd $PBS_O_WORKDIR\n")
            if !gopt[:run]
                script.write("sleep #{sleep}\n")
                sleep += 10
            end
            script.write("cd #{@params[:ID]}_#{job.name}\n")
            # Find last new run made
            newruns = Dir["#{@params[:ID]}_#{job.name}/Events/newrun*"].collect {|x| File.basename(x) if File.directory? x}
            puts newruns
            newruns.delete(nil)
            last = 0
            newruns.each do |x|
                num = x.split("_")[1].to_i
                if num > last
                    last = num 
                end
            end
            script.write("for i in {#{last + 1}..#{last+gopt[:multi_run]}}\ndo\n")
            if gopt[:mcnlo] 
                exec = "./bin/generate_events -ox"
            else 
                exec = "./bin/generate_events newrun_$i -f"
            end
            script.write("#{exec}\n")
            script.write("done")
            script.close
            if !gopt[:test]
                if !gopt[:run]
                    if checksubmit
                        i,o,e,t = Open3.popen3("qsub -e err_generate_#{@params[:ID]}_#{job.name} -o output_generate_#{@params[:ID]}_#{job.name} mg5generate.sh")
                        @procs << o.read
                    end
                else
                    if checkrunning
                        i,o,e,t = Open3.popen3("cd #{@params[:ID]}_#{job.name};  #{exec} > ../output_generate_#{@params[:ID]}_#{job.name}")
                        @procs << t
                        #i,o,e,t = Open3.popen3("echo hola")
                        #@procs << t
                    end
                end
            end
        end
    end
end
