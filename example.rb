#!/bin/env ruby
require_relative  "JobManager"

# This is an exmaple using JobManager to run MadGraph jobs.
# Each job manager manages a set of processes for MC generation
# using MadGraph.
# Each JobManager object can contain a given number of processes.
# A good advice is to have a JobManager for a given MC process and
# define as many generations as needed (e.g. differnt parameters for 
# a given model).
# Works with ruby 1.9

# Lets imagine that we are managing MC production for ttbar background.
VERSION = "13TeV" # This will be used as the version of our production

#Create a JobManager object. The arguments are:
# MANDATORY:
# ID => The ID of process. This is used as global identifier for all jobs inside
#       the manager. 
#
# OPTIONAL:
# submax => Maximum number of jobs to be submited.
# max => Max number of jobs to be run in iterative mode.
# wait => Seconds to wait until refresh information printout.
# verbose => Verbose output (to be improved)

ttbar = JobManager.new(:ID => "ttbar_#{VERSION}", :submax => 10) 
# In this case the ID would be ttbar_13TeV and only 10 jobs will be submited
# simultaneously.

# A clear advantage is the easy it is to set different jobs. One could create an array of
# params cards for different parameters of the model and automatically launch the jobs.
# In this case we are going to produce ttbar in LO and NLO. 
# Also, we are going to launch 10 productions in parallel so it could run simultaneously
# instead of waiting for each one to finish to start another run, as it would do in the 
# multi_run
tags = ["LO","NLO"]
tags.each do |tag| # Loop in tags for LO and NLO
    (1..10).each do |rr| # Loop from 1 to 10
        # The add_job method create a block with a variable on it that will be the job
        # to add. We can customize it inside the block.
        ttbar.add_job do |j|
            # The name is a suffix added to the ID for the output folder 
            # This allows JobManager to create a set of output folder
            # easily indetified by the ID but different for different jobs
            j.name = "#{tag}_#{rr}" # e.g. NLO_10

            # Define processes. 
            # We can insert as many processes as we want in the job.
            # This is donde as you would do in the proc card of Madgraph.
            if tag == "LO" # Processes in LO
                j.procs << "p p > t t~, (t > W+ b, W+ > l+ vl),(t~ > W- b~, W- > l- vl~)"
            else # Process in NLO 
                j.procs << "p p > t t~ [QCD]"
            end
            # The files array are files added to MadGraph. If you add a paramcard, delphes card
            # or madspin card it will be used in the generations.
            j.files << "./delphes_myATLAS.dat"
            j.files << "./madspin_card.dat" if tag == "NLO" # Add madspin only for NLO
            # The options are set with the opts array. Insert any option you would select in the
            # interactive mode of Madgraph
            if tag == "LO" # Activate pythia and delphes for LO
                j.opts << "pythia=ON"
                j.opts << "delphes=ON"
            else # Activate madspin for NLO
                j.opts << "madspin=ON"
            end
            # You could use the multi_run option if available for your run as well.
            #j.opts << "multi_run 20"

            # The set method is used to set any parameter that could be modified in the run_card.
            # Just pass a string containing the parameter and the value.
            j.set "ebeam1 6500"
            j.set "ebeam2 6500"
            j.set "drll 0"
            j.set "drjl 0.2"
            j.set "ptj 10"
            j.set "etal 3"
            # Since we are running 10 copies of the same generation lets use a different
            # seed so each generation is independent.
            j.set "iseed #{Random.rand 100000}"
            j.set "nevents 50000"
            j.set "parton_shower PYTHIA6Q" if tag == "NLO"
        end
    end
end

#Launch jobs
puts "Running ttbar"
# The launch method has different optional arguments
# :test => Simulate the run but don't generate anything. This will create the proc_cards so 
#          you can test that everything is fine.
# :run => If set to false suplied the jobs will be submited to the default queue of the PBS cluster.
#         If set to true it will run in the frontend, no submition.
ttbar.launch(:test => false)

# You can also increase the statistic of an already run job. This have different options:
# (The statistic extension is done to the same number of events generated the first time).
# test => The same as before.
# run => The same as before.
# mcnlo => Set to true if the process is an mc@nlo process.
# multi_run => How many runs do you want to use.

#ttbar.generate_events(:test => false, :mcnlo => true, :multi_run => 3)
