#!/bin/env ruby
require 'open3'
require 'optparse'
require 'ostruct'

# Parse command line $options
$options = OpenStruct.new
$options.tag = ""
$options.order = "nlo"
$options.shower = "PYTHIA6Q"
$options.exe = "Delphes/DelphesSTDHEP"
$options.folders = []
parser = OptionParser.new do |opts|
    opts.banner = "Usage: doDelphes.rb [options]"
    opts.separator "Mandatory arguments"
    opts.on("-c","--card CARD","Delphes card to be used") {|x| $options.card=x}
    opts.on("-f","--folders expr","Output folders to be use.",
            "The one used as output when running Madgraph.", 
            "A glob expresion is supported or a list separated by commas (both between \"\").",String) do |x|
                list = []
                if x.include? "," 
                    list = x.split(",")
                end
                if list.length > 0
                    $options.folders = list
                else
                    $options.folders = Dir[x].collect {|d| d if File.directory? d}
                end
            end
    opts.separator ""
    opts.separator "Optional arguments"
    opts.on("-t","--tag [TAG]","Tag to be added in the output file",
           "\t Default: none") {|x| $options.tag = x}
           opts.on("-o","--order [ORDER]","Perturvative order used to generate the sample (nlo,lo)",["nlo","lo"],
           "\t Default: #{$options.order}") {|x| $options.order = x.downcase}
           opts.on("-s","--shower [SHOWER]","Shower used in the samples generation",
           "\t Default: #{$options.shower}") {|x| $options.shower = x.downcase}
           opts.on("-e","--exec [SHOWER]","Delphes executable",
           "\t Default: #{$options.exe}") {|x| $options.exe = x.downcase}
           opts.on_tail("-h", "--help", "Show this message") do
               puts opts
               exit
           end
end
parser.parse! ARGV
abort "ERROR: No card supplied" if $options.card == nil
abort "ERROR: No folders supplied" if $options.folders == nil


THISDIR = Dir.pwd

def decompress(folder,input)
    #if !File.exist?($input+".gz")
    #    abort "No file events.hep.gz found in #{folder} under usual location\n"
    #end
    #puts "Preparing #{$input}..."
    #Open3.popen3(DELPHES,"delphes_myATLAS2.dat",output,$input) do |i,o,e,t|
    $script.write("cd #{folder}\n")
    $script.write("echo \"Decompressing file...\"\n")
    $script.write("gzip -d #{input}\n")
    $script.write("cd #{THISDIR}\n")
    #script.write("#{DELPHES} delphes_myATLAS2.dat #{output} #{$input}\n")
    #Open3.popen3("decompress.sh") do |i,o,e,t|
    #    o.each_line do |l|
    #        puts l
    #    end
    #end
end

def checkhep(folder, tag)
    gzlist = Dir[folder+"/#{tag}.hep.gz"]
    heplist = Dir[folder+"/#{tag}.hep"]
    return heplist[0] if gzlist.length == 0 and heplist.length == 1
    if gzlist.length > 1
        abort "ERROR: More than one hep.gz file in #{folder}. I don't know which one to use"
    else
        decompress folder,File.basename(gzlist[0])
        return gzlist[0].gsub ".gz",""
    end
    if heplist.length == 0
        abort "ERROR: I don't see any hep.gz or hep file in  #{folder}. Something is wrong"
    end
end

def prepare_hep(folder)
    anfolder = folder.split("/Events")[0]
    file = ""
    if $options.order =="lo"
        file = checkhep folder,"*pythia_events"
    else
        # Check if needs to be copied
        number = File.basename(folder).split("_")[1].to_i.to_s
        tomove = "#{anfolder}/MCatNLO/RUN_#{$options.shower}_#{number}/events.hep.gz"
        if File.exist? tomove
            $script.write("echo Moving file...\n")
            $script.write("mv #{tomove} #{folder}/\n")
            decompress folder,"events.hep.gz"
            file  = "#{folder}/events.hep"
        else
            file = checkhep folder,"events"
        end
    end
    file
end

def list_hepfolders(folder)
    list = []
    if $options.order == "lo"
        list = Dir["#{folder}/Events/*run_*/*hep.gz"].collect{|x| File.dirname x}
        list += Dir["#{folder}/Events/*run_*/*hep"].collect{|x| File.dirname x}
        if list.length == 0
            abort "ERROR (lo): No hep files have been found under #{folder}/Events"
        end
    elsif $options.order == "nlo"
        list = Dir["#{folder}/Events/run_*_decayed_1"].collect{|x| x if File.directory? x}
        list.delete nil
    else
        abort "ERROR: Order supplied does not match any of \"nlo\", \"lo\". Use a valid one"
    end
    list
end

$options.folders.each do |folder|
    puts "Creating Delphes TTree for #{folder}...\n"
    list_hepfolders(folder).each do |ff|
        puts "\t -> #{ff}"
        filename = "delphesjob.sh"
        $script  = File.open(filename,"w")
        `chmod +x #{filename}`
        $script.write("#!/bin/env bash\ncd $PBS_O_WORKDIR\n")
        input = prepare_hep ff
        output = "#{ff}/#{$options.tag}_delphes.root"
        puts "\tInput: #{input}\n\tOutput: #{output}\n"
        if File.exist? output
            $script.write("rm #{output}\n")
        end
        $script.write("#{$options.exe} #{$options.card} #{output} #{input}\n")
        $script.write("echo Done!\n")
        $script.close
        `qsub #{filename}`
    end
end
puts "All jobs submitted succesfully!"
